<?php

$logoExists = FALSE;
$logoPath = getConfigValue('logo_path');
$logoPosition = getConfigValue('logo_position');

if(!empty($logoPath)) {
    $logoExists = TRUE;
}
$output = ($logoExists ? "<img src=\"$logoPath\" alt=\"Logo\">" : "<span>Logo hochladen</span>");
?>

<div id="branding" class="<?php echo $logoPosition ?>">
    <?php echo $output; ?>
</div>