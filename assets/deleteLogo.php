<?php
/**
 * Delete a logo file from the module's asset folder.
 */

require_once '../../../config/glancrConfig.php';
$existingLogoPath = getConfigValue('logo_path');

// Delete the logo file upon request.
if(!empty($existingLogoPath)) {
    $result = unlink(GLANCR_ROOT . $existingLogoPath);
    if ($result) {
        http_response_code('200');
    } else {
        http_response_code('500');
    }
    return;
}