<?php
include('../../../config/glancrConfig.php');

$logoPath = getConfigValue('logo_path');
if(empty($logoPath)) {
	$logoPath = '';
}

echo json_encode($logoPath);
