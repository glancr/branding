<?php
/**
 * Upload a logo file to the module's asset folder.
 */

// Upload the new file.
$file = $_FILES['file'];

if (!isset($file)) {
    print("No file received.");
    http_response_code('400');
    return;
}

if (substr($file['type'], strpos($file['type'], 'image/')) === FALSE) {
    print("Received invalid image file MIME type.");
    http_response_code('415');
    return;
}

if (!is_writable(__DIR__)) {
    print("Upload folder not writable. Check permissions for". __DIR__);
    http_response_code('500');
    return;
}

// All good to go: Delete existing logo file and save the uploaded image.
try {
//    unlink(GLANCR_ROOT . $existingLogoPath);
    $fileInfo = new SplFileInfo($file['name']);
    move_uploaded_file($file['tmp_name'], __DIR__ .'/logo.'. $fileInfo->getExtension());
    $body = [
        "imagePath" => '/modules/branding/assets/logo.'. $fileInfo->getExtension(),
        "error" => NULL
    ];
} catch (Exception $exception) {
    $body = [
        "imagePath" => '',
        "error" => $exception->getMessage()
    ];
}

// Return the new image path for instant display in the backend.
http_response_code('200');
echo json_encode($body);