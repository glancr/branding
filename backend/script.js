/**
 * Backend UI logic for the branding/logo modal.
 */

if (!String.prototype.includes) {
    String.prototype.includes = function(search, start) {
        'use strict';
        if (typeof start !== 'number') {
            start = 0;
        }

        if (start + search.length > this.length) {
            return false;
        } else {
            return this.indexOf(search, start) !== -1;
        }
    };
}

var errorContainer = $('#logoFileError');

// Trigger logo file deletion when the "remove" button is clicked.
$('#branding__preview').on('click', '#branding__remove', function() {

    var previewBox = $('#branding__preview');
    $.post('../modules/branding/assets/deleteLogo.php')
        .done(function() {
            $.post('setConfigValueAjax.php', {'key' : 'logo_path', 'value' : ''});
            previewBox.empty().append("<p>" + BRANDING_LOCALE.noImagePath + "</p><img src='' alt='' />");
        })
        .fail(function () {
            previewBox.append("<p class='error'>" + BRANDING_LOCALE.deletionError + "</p>");
        });
});

// Update config when the alignment option changes.
$('#branding_position').change(function() {
    $.post('setConfigValueAjax.php', {'key' : 'logo_position', 'value' : $('#branding_position').val()});
    $('#ok').show(30, function() {
        $(this).hide('slow');
    })
});

// Uoload new logo images & handle preview.
$('#branding__edit').click(function() {

    var logoFileInput = $('#branding_image'),
        uploadButton = $('#branding__edit'),
        file = logoFileInput[0].files[0];

    if(logoFileInput.val() === '') {
        errorContainer.text(BRANDING_LOCALE.notAnImage).animate({
            opacity: 0
        }, 3000, function() {
            errorContainer.text('');
            errorContainer.css('opacity',1);
        });

        return;
    }

    // Test if the chosen file has a valid image MIME type.
    if(!file.type.includes('image/')) {

        // Clear the input.
        logoFileInput.val('');

        //@TODO: localize error message
        $('#logoFileError').text(BRANDING_LOCALE.invalidMimeType).animate({
            opacity: 0
        }, 3000, function() {
            errorContainer.text('');
            errorContainer.css('opacity',1);
        });

        return;

    }

    // Code below gets executed if the above tests pass.

    // Handle UI upload state: Disabled button, loading spinner.
    uploadButton.attr("disabled", true);
    uploadButton.addClass('loading__button');
    var initialText = uploadButton.text();
    uploadButton.html('<div class="loading__button--inner"></div>');

    var fd = new FormData();
    fd.append("file", file);
    $.ajax({
        url: logoFileInput.parent('form').attr('action'),
        type: "POST",
        data: fd,
        processData: false,
        contentType: false,
        success: function(response) {

            // If the file upload was successful, save the path in config and display a preview.
            var imagePath = JSON.parse(response).imagePath;
            $.post('setConfigValueAjax.php', {'key' : 'logo_path', 'value' : imagePath});
            $('#branding__preview').empty().append(
                "<p>Aktuell gewähltes Logo:</p>" +
                "<img src=\"" + imagePath + " \" alt=\"Logo\" />" +
                "<button id='branding__remove'>Entfernen</button>"
            );

            $('#ok').show(30, function() {
                $(this).hide('slow');
            })
        },
        error: function (response) {
            errorContainer.text(response.body);
            $('#error').show(30, function() {
                $(this).hide('slow');
            })
        },
        complete: function () {
            logoFileInput.val('');
            uploadButton.attr("disabled", false);
            uploadButton.html(initialText);
            uploadButton.removeClass('loading__button');
        }
    })
});