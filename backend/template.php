<?php

// Make sure these strings are available in the translation files, since they're partly derived from variables at runtime.
_('branding_title');
_('branding_description');

_('left');
_('center');
_('right');

$logoExists = FALSE;

$logoPath = getConfigValue('logo_path');
if(!empty($logoPath)) {
    $logoExists = TRUE;
}

$logoPosition = getConfigValue('logo_position');

?>

<p>Lade hier dein Logo als Bilddatei (JPEG, PNG, GIF) hoch, um es auf deinem glancr anzuzeigen!</p>

<div id="branding__preview">
<?php print($logoExists ?
    "<p>Aktuell gewähltes Logo:</p><img src=\"$logoPath\" alt=\"Logo\"><button id='branding__remove' type='button'>Entfernen</button>"
    :
    "<p>". _('No logo set. Upload an image.') ."</p><img src=\"\" alt='' />");
?>
</div>

<h5>Logo ausrichten</h5>
<label for="branding_position">Ausrichtung des Logos</label>
<select name="brandingPosition" id="branding_position">
    <?php
        foreach (['left', 'center', 'right'] as $option) {
            if ($option === $logoPosition) {
                print("<option selected value=\"$option\">". _($option));
            } else {
                print("<option value=\"$option\">". _($option));
            }
        }
    ?>
</select>

<h5>Neues Logo hochladen</h5>
<form action="../modules/branding/assets/uploadLogo.php">
    <input type="file" id="branding_image" name="brandingImage" accept="image/*" class="">
</form>

<div id="logoFileError"></div>

<div class="block__add" id="branding__edit">
	<button class="branding-logo__edit--button" href="#">
		<span><?php echo _('save'); ?></span>
	</button>
</div>

<script type="text/javascript">
    var BRANDING_LOCALE = {
        "noImagePath": "<?php echo _('No logo set. Upload an image.') ?>",
        "invalidMimeType": "<?php echo _('Please choose a valid image file (JPEG, PNG, GIF).') ?>",
        "notAnImage": "<?php echo _('Please choose an image file from your hard disk.') ?>"
    }
</script>